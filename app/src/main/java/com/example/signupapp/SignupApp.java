package com.example.signupapp;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by nagaraj.l on 24/08/15.
 */
public class SignupApp extends Application {

    private RequestQueue mRequestQueue;
    private static SignupApp sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mRequestQueue = Volley.newRequestQueue(this);
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public synchronized static SignupApp getInstance() {
        return sInstance;
    }
}
