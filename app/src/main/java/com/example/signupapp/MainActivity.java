package com.example.signupapp;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity {

    NetworkRequestManager mNetworkReqManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNetworkReqManager = new NetworkRequestManager();
        final View view = findViewById(R.id.signup_details);
        findViewById(R.id.button_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndSave(view);
            }
        });
    }

    private void validateAndSave(View view) {

        String email = ((EditText)view.findViewById(R.id.personal_emaid_address)).getText().toString();
        String password = ((EditText)view.findViewById(R.id.password)).getText().toString();
        String rePassword = ((EditText)view.findViewById(R.id.reenter_password)).getText().toString();

        String fName = ((EditText)view.findViewById(R.id.first_name)).getText().toString();
        String lName = ((EditText)view.findViewById(R.id.last_name)).getText().toString();

        String dob = ((EditText)view.findViewById(R.id.dob)).getText().toString();
        String mobNum = ((EditText)view.findViewById(R.id.mobile_phone)).getText().toString();

        User user =
                new User.Builder(email,password).setRePassword(rePassword).
                        setFirstName(fName).setLastName(lName).setMobileNumber(mobNum).setDOB(dob).build();

        if(user != null){
            mNetworkReqManager.signUpUser(user);
        }else{
            Toast.makeText(getApplicationContext(),"Please fill all the details",Toast.LENGTH_SHORT).show();
        }


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
