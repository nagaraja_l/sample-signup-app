package com.example.signupapp;

import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nagaraj.l on 24/08/15.
 */
public class NetworkRequestManager {

    private static String SIGNUP_URL = "http://int.eclinic247.com/api/info/test/save";


    public void signUpUser(User user) {

        {


            Map<String, String> postParam= new HashMap<String, String>();
            postParam.put("email", user.getmEmail());
            postParam.put("firstName", user.getmFName());
            postParam.put("lastName", user.getmLName());
            postParam.put("dob", user.getmDob());
            postParam.put("phone", user.getmMobilePhone());


            // Create a new JsonObjectRequest.
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, SIGNUP_URL,
                    new JSONObject(postParam),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject mresponse) {
                            Toast.makeText(SignupApp.getInstance(), "User Signed Up successfully", Toast.LENGTH_SHORT).show();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(SignupApp.getInstance(), "Error SigningUp the user", Toast.LENGTH_SHORT).show();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }
            };


            // With the request created, simply add it to our Application's RequestQueue
            SignupApp.getInstance().getRequestQueue().add(request);
        }
    }
}
