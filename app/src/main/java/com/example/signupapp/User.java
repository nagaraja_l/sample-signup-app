package com.example.signupapp;

import android.text.TextUtils;

/**
 * Created by nagaraj.l on 22/08/15.
 */
public class User {

    String mEmail;
    String mPassword;
    String mRePassword;
    String mFName;
    String mLName;
    String mDob;
    String mMobilePhone;

    public User(Builder builder) {

        mEmail = builder.mEmail;
        mPassword = builder.mPassword;
        mRePassword = builder.mRePassword;
        mFName = builder.mFName;
        mLName = builder.mLName;
        mDob = builder.mDob;
        mMobilePhone = builder.mMobilePhone;

    }

    public String getmEmail() {
        return mEmail;
    }

    public String getmFName() {
        return mFName;
    }

    public String getmLName() {
        return mLName;
    }

    public String getmDob() {
        return mDob;
    }

    public String getmMobilePhone() {
        return mMobilePhone;
    }

    public static class Builder {

        String mEmail;
        String mPassword;
        String mRePassword;
        String mFName;
        String mLName;
        String mDob;
        String mMobilePhone;

        public Builder(String email, String Password) {
            this.mEmail = email;
            this.mPassword = Password;
        }

        public Builder setRePassword(String rePassword) {
            this.mRePassword = rePassword;
            return this;
        }

        public Builder setFirstName(String fName) {
            this.mFName = fName;
            return this;
        }

        public Builder setLastName(String lName) {
            this.mLName = lName;
            return this;
        }

        public Builder setDOB(String dob) {
            this.mDob = dob;
            return this;
        }

        public Builder setMobileNumber(String num) {
            this.mMobilePhone = num;
            return this;
        }

        private boolean validate(Builder builder) {

            return (!TextUtils.isEmpty(builder.mEmail) && !TextUtils.isEmpty(builder.mPassword) &&
                    !TextUtils.isEmpty(builder.mRePassword) && builder.mPassword.equals(builder.mRePassword));
        }

        public User build() {
            if (validate(this)) {
                return new User(this);
            } else {
                return null;
            }
        }
    }
}
